package br.com.itau.gateway;

import org.springframework.stereotype.Component;

@Component
public class FilmeClientFallback implements FilmeClient{
	
	public String buscarFilme() {
		return "Fuga das Galinhas";
	}
}
